package com.devcamp.s55.pizza365;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CDailyCampaign {
    @CrossOrigin
    @GetMapping("/pizza-campaign")

    public String getCampaignMessage() {
        // Lấy ngày hôm nay là thứ mấy:
        VietDate dateObj = new VietDate();
        String date = dateObj.getDateViet();
        String message = "Hôm nay là " + date + ", ";
        
        if (date.indexOf("Thứ Hai") == 0) {
            message += "cho ăn thiếu";
        }
        else if (date.indexOf("Thứ Ba") == 0) {
            System.out.println("thứ ba,");
            message += "giữ xe miễn phí";
        }
        else if (date.indexOf("Thứ Tư") == 0) {
            message += "ăn pizza tặng tô phở";
        }
        else if (date.indexOf("Thứ Năm") == 0) {
            message += "mua 1 tặng 5";
        }
        else if (date.indexOf("Thứ Sáu") == 0) {
            message += "ăn khỏi trả tiền";
        }
        else if (date.indexOf("Thứ Bảy") == 0) {
            message += "không có gì";
        }
        else if (date.indexOf("Chủ Nhật") == 0) {
            message += "quán nghỉ";
        }

        return message;
    }


}
