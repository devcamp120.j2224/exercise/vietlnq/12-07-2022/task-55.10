package com.devcamp.s55.pizza365;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class CMenuLarge {
    @CrossOrigin
    @GetMapping("/pizza-menu-l")

    public String getMenu () {
        String largeMenu = "{\"duongKinh\":30,\"suonNuong\":8,\"salad\":500,\"nuocNgot\":4,\"giaTien\":250000}";
        return largeMenu;
    }
}