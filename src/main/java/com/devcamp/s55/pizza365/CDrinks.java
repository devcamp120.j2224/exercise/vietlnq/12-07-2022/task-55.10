package com.devcamp.s55.pizza365;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CDrinks {
    @CrossOrigin
    @GetMapping("pizza-drinks")

    public String getDrinkList() {
        String drinkList = "[{\"maNuocUong\":\"TRATAC\",\"tenNuocUong\":\"Trà tắc\",\"donGia\":10000,\"ghiChu\":null,\"ngayTao\":1615177934000,\"ngayCapNhat\":1615177934000},{\"maNuocUong\":\"COCA\",\"tenNuocUong\":\"Cocacola\",\"donGia\":15000,\"ghiChu\":null,\"ngayTao\":1615177934000,\"ngayCapNhat\":1615177934000},{\"maNuocUong\":\"PEPSI\",\"tenNuocUong\":\"Pepsi\",\"donGia\":15000,\"ghiChu\":null,\"ngayTao\":1615177934000,\"ngayCapNhat\":1615177934000},{\"maNuocUong\":\"LAVIE\",\"tenNuocUong\":\"Lavie\",\"donGia\":5000,\"ghiChu\":null,\"ngayTao\":1615177934000,\"ngayCapNhat\":1615177934000},{\"maNuocUong\":\"TRASUA\",\"tenNuocUong\":\"Trà sữa trân châu\",\"donGia\":40000,\"ghiChu\":null,\"ngayTao\":1615177934000,\"ngayCapNhat\":1615177934000},{\"maNuocUong\":\"FANTA\",\"tenNuocUong\":\"Fanta\",\"donGia\":15000,\"ghiChu\":null,\"ngayTao\":1615177934000,\"ngayCapNhat\":1615177934000}]";
        return drinkList;
    }
}
