package com.devcamp.s55.pizza365;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class CMenuMedium {
    @CrossOrigin
    @GetMapping("/pizza-menu-m")

    public String getMenu () {
        String mediumMenu = "{\"duongKinh\":25,\"suonNuong\":4,\"salad\":300,\"nuocNgot\":3,\"giaTien\":200000}";
        return mediumMenu;
    }
}