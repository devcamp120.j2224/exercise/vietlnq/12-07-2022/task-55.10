package com.devcamp.s55.pizza365;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class CMenuSmall {
    @CrossOrigin
    @GetMapping("/pizza-menu-s")

    public String getMenu () {
        String smallMenu = "{\"duongKinh\":20,\"suonNuong\":2,\"salad\":200,\"nuocNgot\":2,\"giaTien\":150000}";
        return smallMenu;
    }
}
